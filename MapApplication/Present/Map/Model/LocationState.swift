//
//  LocationState.swift
//  MapApplication
//
//  Created by Максим Романов on 02.10.2020.
//

import Foundation
import GoogleMaps

struct MapState {
    var currentLocation: CLLocationCoordinate2D!
    var standartPath = GMSMutablePath()
    
    var path: [Path] = []
    
    var geoLocationWay: [DefaultInfo] = [
        DefaultInfo(location: [55.754529, 37.623133], time: "9:01"),
        DefaultInfo(location: [55.754529, 37.623133], time: "9:02"),
        DefaultInfo(location: [55.755855, 37.620436], time: "9:03"),
        DefaultInfo(location: [55.755523, 37.619736], time: "9:04"),
        DefaultInfo(location: [55.755109, 37.619082], time: "9:05"),
        DefaultInfo(location: [55.754668, 37.618293], time: "9:06"),
        DefaultInfo(location: [55.754934, 37.617408], time: "9:07"),
        DefaultInfo(location: [55.755423, 37.616224], time: "9:08"),
        DefaultInfo(location: [55.756147, 37.615541], time: "9:09"),
        DefaultInfo(location: [55.755525, 37.613835], time: "9:10"),
        DefaultInfo(location: [55.754432, 37.612387], time: "9:13"),
        DefaultInfo(location: [55.754124, 37.613621], time: "9:16"),
        DefaultInfo(location: [55.753707, 37.614973], time: "9:18"),
        DefaultInfo(location: [55.752358, 37.614479], time: "9:20"),
        DefaultInfo(location: [55.750985, 37.614597], time: "9:22"),
        DefaultInfo(location: [55.749300, 37.614150], time: "9:26"),
        DefaultInfo(location: [55.749886, 37.617498], time: "9:27"),
        DefaultInfo(location: [55.751178, 37.617467], time: "9:30"),
        DefaultInfo(location: [55.751963, 37.619420], time: "9:33"),
        DefaultInfo(location: [55.753389, 37.621838], time: "9:35"),
        DefaultInfo(location: [55.752229, 37.624519], time: "9:37"),
        DefaultInfo(location: [55.751656, 37.625463], time: "9:40"),
        DefaultInfo(location: [55.750454, 37.625849], time: "9:42"),
        DefaultInfo(location: [55.750212, 37.629647], time: "9:44"),
        DefaultInfo(location: [55.750635, 37.631932], time: "9:50"),
        DefaultInfo(location: [55.751945, 37.632200], time: "10:00"),
        DefaultInfo(location: [55.752796, 37.629657], time: "10:03"),
        DefaultInfo(location: [55.754597, 37.628440], time: "10:07"),
        DefaultInfo(location: [55.754229, 37.626420], time: "10:10"),
        DefaultInfo(location: [55.753360, 37.625776], time: "10:15"),]
}

struct Path {
    var location: CLLocationCoordinate2D
    var time: String
}

struct DefaultInfo {
    var location: [CLLocationDegrees]
    var time: String
}
