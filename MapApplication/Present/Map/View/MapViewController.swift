//
//  ViewController.swift
//  MapApplication
//
//  Created by Максим Романов on 02.10.2020.
//

import UIKit
import SnapKit
import GoogleMaps

class MapViewController: UIViewController {

    private var mapView = GMSMapView()
    private var bottomView: BottonViewProtocol!
    
    var presenter: MapPresenterProtocol!
    
    var bounds = GMSCoordinateBounds()
    var progressLine = GMSPolyline()
    var progressPath = GMSMutablePath()
    
    private var iPosition = 0
    private var marker = GMSMarker()
    private var timer = Timer()
    private var lock: Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = .white
        setupSubviews()
        actionHander()
        presenter.countStandartPath()
        self.drawPolyline()
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        setupConstraints()
    }
    
    func config(presenter: MapPresenterProtocol) {
        self.presenter = presenter
    }
    
    private func actionHander() {
        bottomView.actionHandler = { [weak self] status in
            guard let self = self else {return}
            switch status {
            case .playing:
                self.setTimer()
            case .stoped:
                self.stopTimer()
            }
        }
    }
    
    private func setupSubviews() {
        bottomView = BottonView()
        
        let camera = GMSCameraPosition.camera(withLatitude: 45.040620, longitude: 38.975249, zoom: 14)
        mapView = GMSMapView.map(withFrame: self.view.frame, camera: camera)
        
        let icons = UIImageView(image: UIImage())
        icons.frame.size = CGSize(width: 20, height: 20)
        icons.layer.cornerRadius = 10
        
        marker.groundAnchor = CGPoint(x: CGFloat(0.5), y: CGFloat(0.5))
        marker.position = presenter.getCurrentLocation()
        progressLine.updateColor(color: #colorLiteral(red: 0.5737789273, green: 0, blue: 0.04922301322, alpha: 1), duration: 0.2)
        marker.iconView = icons
        marker.iconView?.updateColor(color: #colorLiteral(red: 0.5725490451, green: 0, blue: 0.2313725501, alpha: 1) ,duration: 0.2)
        marker.icon = UIImage()
        marker.map = mapView
        mapView.selectedMarker = marker
        
        self.view.addSubview(mapView)
        self.view.addSubview(bottomView as! UIView)
        
    }

    private func setupConstraints() {
        mapView.snp.makeConstraints { (make) in
            make.edges.equalToSuperview()
        }
        (bottomView as! UIView).snp.makeConstraints { (make) in
            make.bottom.equalToSuperview().inset(UIApplication.shared.keyWindow!.safeAreaInsets.bottom)
            make.left.equalToSuperview()
            make.right.equalToSuperview()
        }
    }
    
    private func stopTimer() {
        timer.invalidate()
        progressLine.updateColor(color: #colorLiteral(red: 0.5725490451, green: 0, blue: 0.2313725501, alpha: 1),duration: 0.2)
        marker.iconView?.updateColor(color: #colorLiteral(red: 0.5725490451, green: 0, blue: 0.2313725501, alpha: 1), duration: 0.2)
        lock = true
    }
    
    private func setTimer(){
        lock = false
        timer = Timer.scheduledTimer(withTimeInterval: 0.4, repeats: true, block: { [weak self] _ in
            self?.playAnimation()
            print("Counting")
        })
    }
    
    func playAnimation() {
        guard !lock else { return }
        
        if iPosition < presenter.getStandartPath().count() {
            CATransaction.begin()
            CATransaction.setValue(0.08, forKey: kCATransactionAnimationDuration)
            marker.position = presenter.getStandartPath().coordinate(at: UInt(iPosition))
            progressPath.add(presenter.getStandartPath().coordinate(at: UInt(iPosition)))
            progressLine.path = progressPath
            progressLine.strokeWidth = 5
            progressLine.updateColor(color: #colorLiteral(red: 0, green: 0.5985788107, blue: 0.9598628879, alpha: 1), duration: 0.2)
            marker.iconView?.updateColor(color: #colorLiteral(red: 0, green: 0.5985788107, blue: 0.9598628879, alpha: 1), duration: 0.2)
            progressLine.map = mapView
            marker.title = presenter.getCurrentTime(index: iPosition)
            CATransaction.commit()
            iPosition += 1
        } else {
            CATransaction.begin()
            CATransaction.setValue(0.3, forKey: kCATransactionAnimationDuration)
            marker.position = presenter.getCurrentLocation()
            timer.invalidate()
            progressLine.map = nil
            progressPath.removeAllCoordinates()
            iPosition = 0
            progressLine.path = nil
            marker.title = ""
            bottomView.setStatus(status: .stoped)
            marker.iconView?.updateColor(color: #colorLiteral(red: 0.5725490451, green: 0, blue: 0.2313725501, alpha: 1), duration: 1.5)
            CATransaction.commit()
        }
        
    }
    
    func drawPolyline() {
        
        let polyline = GMSPolyline(path: presenter.getStandartPath())
        polyline.strokeColor = UIColor.lightGray
        polyline.strokeWidth = 5.0
        polyline.map = self.mapView
        
        let bounds = GMSCoordinateBounds(path: presenter.getStandartPath())
        self.mapView.animate(with: GMSCameraUpdate.fit(bounds, withPadding: 150.0))
    }
    
    func getHeadingForDirection(fromCoordinate fromLoc: CLLocationCoordinate2D, toCoordinate toLoc: CLLocationCoordinate2D) -> Float {
        
        let fLat: Float = Float((fromLoc.latitude))
        let fLng: Float = Float((fromLoc.longitude).degreesToRadians)
        let tLat: Float = Float((toLoc.latitude).degreesToRadians)
        let tLng: Float = Float((toLoc.longitude).degreesToRadians)
        let degree: Float = (atan2(sin(tLng - fLng) * cos(tLat), cos(fLat) * sin(tLat) - sin(fLat) * cos(tLat) * cos(tLng - fLng))).radiansToDegrees
        if degree >= 0 {
            return degree - 180.0
        }
        else {
            return (360 + degree) - 180
        }
    }
}

extension FloatingPoint {
    var degreesToRadians: Self { return self * .pi / 180 }
    var radiansToDegrees: Self { return self * 180 / .pi }
}


