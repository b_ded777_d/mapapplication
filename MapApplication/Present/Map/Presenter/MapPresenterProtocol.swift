//
//  MapPresenterProtocol.swift
//  MapApplication
//
//  Created by Максим Романов on 02.10.2020.
//

import Foundation
import GoogleMaps

protocol MapPresenterProtocol {
    func getCurrentLocation() -> CLLocationCoordinate2D
    func getCurrentTime(index: Int) -> String
    func getStandartPath() -> GMSMutablePath
    func countStandartPath()
}
