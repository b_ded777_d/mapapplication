//
//  MapPresenterImp.swift
//  MapApplication
//
//  Created by Максим Романов on 02.10.2020.
//

import Foundation
import GoogleMaps

class MapPresenterImp: MapPresenterProtocol {
    
    private var state: MapState
    
    init(state: MapState) {
        self.state = state
        obtaineCurrentLocaion()
    }
    
    private func obtaineCurrentLocaion() {
        state.currentLocation = CLLocationCoordinate2D(latitude: state.geoLocationWay.first!.location.first!, longitude: state.geoLocationWay.first!.location.last!)
    }
    
    private func transformLocationToPath() {
        state.path = state.geoLocationWay.map({Path(location: CLLocationCoordinate2D(latitude: $0.location.first!, longitude: $0.location.last!), time: $0.time)})
    }
    private func transformPathToStandartPath() {
        DispatchQueue.global().sync {
            transformLocationToPath()
        }
        for path in state.path {
            state.standartPath.add(path.location)
        }
    }
    
    func countStandartPath() {
        DispatchQueue.global().sync {
            transformPathToStandartPath()
        }
    }
    
    func getStandartPath() -> GMSMutablePath {
        return state.standartPath
    }
    
    func getCurrentLocation() -> CLLocationCoordinate2D {
        return state.currentLocation
    }
    func getCurrentTime(index: Int) -> String {
        guard state.path.count > index else { return "" }
        return state.path[index].time
    }
}

