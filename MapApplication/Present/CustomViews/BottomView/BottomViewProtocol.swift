//
//  BottomViewProtocol.swift
//  MapApplication
//
//  Created by Максим Романов on 02.10.2020.
//

import Foundation

protocol BottonViewProtocol {
    var actionHandler: ((ProgressStatus) -> Void)! { get set }
    func setStatus(status: ProgressStatus)
}

enum ProgressStatus {
    case playing
    case stoped
}
