//
//  BottomViewImp.swift
//  MapApplication
//
//  Created by Максим Романов on 02.10.2020.
//

import UIKit

class BottonView: UIView, BottonViewProtocol {
    
    private var containerView: UIView!
    private var button: UIButton!
    private var status: ProgressStatus = .playing
    
    var actionHandler: ((ProgressStatus) -> Void)!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupSubviews()
        setupConstraints()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setStatus(status: ProgressStatus) {
        self.status = status
        customizedButton()
    }
    
    private func setupSubviews() {
        containerView = UIView()
        button = UIButton()
        button.setTitle("Play", for: .normal)
        
        self.addSubview(containerView)
        containerView.addSubview(button)
        button.backgroundColor = #colorLiteral(red: 0, green: 0.5985788107, blue: 0.9598628879, alpha: 1)
        button.titleLabel?.textAlignment = .center
        button.addTarget(self, action: #selector(buttonToched), for: .touchDown)
    }
    
    
    
    @objc func buttonToched() {
        guard actionHandler != nil else { return }
        actionHandler(status)
        defer {
            customizedButton()
        }
    }
    
    private func customizedButton() {
        switch status {
        case .playing:
            status = .stoped
            button.setTitle("Stop", for: .normal)
            button.updateColor(color: #colorLiteral(red: 0.5725490451, green: 0, blue: 0.2313725501, alpha: 1), duration: 1)
        case .stoped:
            status = .playing
            button.setTitle("Play", for: .normal)
            button.updateColor(color: #colorLiteral(red: 0, green: 0.5985788107, blue: 0.9598628879, alpha: 1), duration: 1)
        }
    }
    
    private func setupConstraints() {
        containerView.snp.makeConstraints { (make) in
            make.edges.equalToSuperview()
        }
        button.snp.makeConstraints { (make) in
            make.top.equalToSuperview().inset(16)
            make.left.equalToSuperview().inset(16)
            make.right.equalToSuperview().inset(16)
            make.bottom.equalToSuperview().inset(16)
        }
        button.titleLabel?.snp.makeConstraints({ (make) in
            make.edges.equalToSuperview().inset(10)
        })
    }
}

