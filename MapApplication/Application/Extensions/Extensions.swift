//
//  Extensions.swift
//  MapApplication
//
//  Created by Максим Романов on 02.10.2020.
//

import Foundation
import GoogleMaps

extension UIView {
    func updateColor(color: UIColor,duration: Double) {
        UIView.animate(withDuration: duration) {
            self.backgroundColor = color
        }
    }
}

extension GMSPolyline {
    func updateColor(color: UIColor, duration: Double) {
        UIView.animate(withDuration: duration) {
            self.strokeColor = color
        }
    }
}
